package com.example.scriptrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.function.Function;

@SpringBootApplication
public class ScriptrunnerApplication {

    @Configuration
    public class UsageCostProcessor {

        @Bean
        public Function<Object, Object> processUsageCost() {
            return input -> new BigDecimal(13);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(ScriptrunnerApplication.class, args);
    }

}
